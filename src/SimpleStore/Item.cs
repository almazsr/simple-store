﻿namespace SimpleStore
{
    public class Item
    {
        public string Name { get; set; }

        public string BarCode { get; set; }

        public decimal Price { get; set; }
    }
}
