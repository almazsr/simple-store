using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace SimpleStore.Tests
{
    public class BasketTests
    {
        /// <summary>
        /// Добавить товары в корзину, завершить транзакции без оплаты - выброситься ожидаемое исключение 
        /// </summary>
        [Fact]
        public void AddItems_CompleteWithoutPaying_ThrowsExpectedException()
        {
            // TODO: 
        }

        /// <summary>
        /// Добавить товар в корзину и удалить этот товар - итог и подытог не должны измениться
        /// </summary>
        [Fact]
        public void AddItemAndRemove_Totals_DidntChange()
        {
            // TODO: 
        }

        /// <summary>
        /// Добавить товары в корзину и применить скидку - итог и подытог должен быть ожидаемым
        /// </summary>
        [Theory]
        [MemberData(nameof(Data_AddItemsAndApplyDiscount_Totals_AreExpected))]
        public void AddItemsAndApplyDiscount_Totals_AreExpected(Tuple<Item, int>[] itemsAndQuantity, decimal discount, decimal expectedSubTotal, decimal expectedTotal)
        {
            // TODO: 
        }

        /// <summary>
        /// Завершить транзакцию, добавить товар - выброситься исключение (потому что завершенную транзакцию нельзя модифицировать)
        /// </summary>
        [Fact]
        public void Complete_AddItem_ThrowsException()
        {
            // TODO: 
        }

        /// <summary>
        /// Добавить товары в корзину - итог и подытог должен быть ожидаемым
        /// </summary>
        [Fact]
        public void AddItems_Totals_AreExpected()
        {
            // Нам нужны 3 мока
            var mockIdGenerator = new Mock<IIdGenerator>();
            var mockRepository = new Mock<IItemRepository>();
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();

            // Настраиваем мок репозитория
            // Пусть GetItemByBarCode для баркода "1" item возвращает Item { Name = "Хлеб", Price = 30 }
            mockRepository.Setup(r => r.GetItemByBarCode(It.Is<string>(x => x == "1"))).Returns(new Item { Name = "Хлеб", Price = 30 });
            // Пусть GetItemByBarCode для баркода "2" item возвращает Item { Name = "Жвачка", Price = 10 }
            mockRepository.Setup(r => r.GetItemByBarCode(It.Is<string>(x => x == "2"))).Returns(new Item { Name = "Жвачка", Price = 10 });

            var basket = new Basket(mockRepository.Object, mockCurrentDateTimeProvider.Object, mockIdGenerator.Object);

            // Добавляем товар с баркодом "1" в количестве 1 шт. в корзину
            basket.AddItem("1", 1);

            // Добавляем товар с баркодом "2" в количестве 2 шт. в корзину
            basket.AddItem("2", 2);

            Assert.Equal(50, basket.SubTotal);
            Assert.Equal(50, basket.Total);
        }

        /// <summary>
        /// (Тест с несколькими входным данными)
        /// Купить товары (добавить в корзину, применить скидку, оплатить и завершить) - метод генерирующий чек должен вернуть ожидаемый результат
        /// </summary>
        /// <param name="itemsAndQuantity">Массив товаров и количества</param>
        /// <param name="discount">Скидка</param>
        /// <param name="expectedReceipt">Ожидаемый чек</param>
        [Theory]
        [MemberData(nameof(Data_Buy_GenerateReceipt_ReturnsExpected))]
        public void Buy_GenerateReceipt_ReturnsExpected(Tuple<Item, int>[] itemsAndQuantity, decimal discount, string expectedReceipt)
        {
            // Нам нужны 3 мока
            var mockIdGenerator = new Mock<IIdGenerator>();
            var mockRepository = new Mock<IItemRepository>();
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();

            // Настраиваем мок провайдера текущей даты
            // Пусть метод Get (возвращающий текущую дату возвращает нам всегда 01/01/2020)
            mockCurrentDateTimeProvider.Setup(c => c.Get()).Returns(new DateTime(2020, 1, 1));

            // Настраиваем мок генератора идентификаторов
            // Пусть метод GenerateId возвращает всегда "TestId"
            mockIdGenerator.Setup(r => r.GenerateId()).Returns("TestId");

            // Настраиваем мок репозитория
            // Используем тестовые данные
            foreach (var item in itemsAndQuantity)
            {
                // Пусть GetItemByBarCode для баркода из item возвращает его же
                mockRepository.Setup(r => r.GetItemByBarCode(It.Is<string>(x => x == item.Item1.BarCode))).Returns(item.Item1);
            }

            // Создаем корзину и передаем конструктору на вход созданные нами моки
            var basket = new Basket(mockRepository.Object, mockCurrentDateTimeProvider.Object, mockIdGenerator.Object);

            // Добавляем товары в корзину
            foreach (var item in itemsAndQuantity)
            {
                // Баркод и количество берем из тестовых данных
                basket.AddItem(item.Item1.BarCode, item.Item2);
            }

            // Применяем скидку из тестовых данных
            basket.ApplyDiscount(discount);

            // Платим столько, сколько нужно
            basket.Pay(basket.BalanceDue);

            // Завершаем транзакцию, чтобы сгенерировать чек
            basket.Complete();

            // Генерируем чек
            var actualReceipt = basket.GenerateReceipt();

            // Заменяем символы окончания строки на заданные в системе
            expectedReceipt = expectedReceipt.Replace("\n", Environment.NewLine);

            // Сравниваем ожидаемый результат вывода чека с фактическим
            Assert.Equal(expectedReceipt, actualReceipt);
        }

        public static IEnumerable<object[]> Data_Buy_GenerateReceipt_ReturnsExpected()
        {
            yield return new object[]
            {
                new Tuple<Item, int>[] // товары и количество
                {
                    new Tuple<Item, int>(new Item { BarCode = "1", Name = "Хлеб", Price = 30 }, 1),
                    new Tuple<Item, int>(new Item { BarCode = "2", Name = "Жвачка", Price = 10 }, 2)
                },
                0m, // скидка
                "Корзина #TestId\n" +  // ожидаемый чек
                "Дата: 01/01/2020 00:00:00\n" +
                "______________________\n" +
                "Хлеб: 30.00€ x 1 шт = 30.00€\n" +
                "Жвачка: 10.00€ x 2 шт = 20.00€\n" +
                "______________________\n" +
                "Подытог: 50.00€\n" +
                "______________________\n" +
                "Итог: 50.00€\n" +
                "______________________\n" +
                "Спасибо за покупку!\n"
            };
            yield return new object[]
            {
                new Tuple<Item, int>[] // товары и количество
                {
                    new Tuple<Item, int>(new Item { BarCode = "1", Name = "Пиво", Price = 50 }, 5),
                    new Tuple<Item, int>(new Item { BarCode = "2", Name = "Семечки", Price = 20 }, 2),
                    new Tuple<Item, int>(new Item { BarCode = "3", Name = "Хлеб", Price = 10 }, 1)
                },
                0.2m, // скидка
                "Корзина #TestId\n" +  // ожидаемый чек
                "Дата: 01/01/2020 00:00:00\n" +
                "______________________\n" +
                "Пиво: 50.00€ x 5 шт = 250.00€\n" +
                "Семечки: 20.00€ x 2 шт = 40.00€\n" +
                "Хлеб: 10.00€ x 1 шт = 10.00€\n" +
                "______________________\n" +
                "Подытог: 300.00€\n" +
                "______________________\n" +
                "Скидка: 20%\n" +
                "______________________\n" +
                "Итог: 240.00€\n" +
                "______________________\n" +
                "Спасибо за покупку!\n"
            };
        }

        public static IEnumerable<object[]> Data_AddItemsAndApplyDiscount_Totals_AreExpected()
        {
            yield return new object[]
            {
                new Tuple<Item, int>[] // товары и количество
                {
                    new Tuple<Item, int>(new Item { BarCode = "1", Name = "Хлеб", Price = 30 }, 1),
                    new Tuple<Item, int>(new Item { BarCode = "2", Name = "Жвачка", Price = 10 }, 2)
                },
                0.1m, // скидка
                50m,
                45m
            };
            // TODO: Добавить еще тестовых данных
        }
    }
}
